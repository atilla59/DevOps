
[http://138.68.84.22 işlemlerin gerçekleştirildiği uzak makine](http://138.68.84.22)
## ansible kurulumu
    - ilk olarak kişisel bilgisayarıma ansible kurdum
    - daha sonra digitalocean uzak sunucumu ansible inventory olarak ekledim
    - ssh-copy-id ve ss-keygen oluşturdum
    - tüm işlemler http://138.68.84.22/ adresinde gerçekleştirildi ödev kontrolü için istenildiğinde sunucu ssh bilgilerini verebilirim header check için yine bu adres kullanulabilir.

# 2.1. DB kullanan bir uygulamayı dockerized edip, nginx üzerinden serve edilmesi
    - db kullanan bir uygulama için wordpress tercih ettim
      - https://hub.docker.com/_/wordpress
      - https://hub.docker.com/_/mysql
      - https://hub.docker.com/_/nginx
    -  wordpress'i ansible ile deploy edebilmek için task olarak (yum utils,epel-release,docker,pip ve docker compose) tasklerini ekledim roles/docker/tasks/main.yml klasörüne bakılabilir.
    -  daha sonra (wordpress,nginx,mysql) stack'ini kullanabilmek için docker-compose.yml dosyamı template formatında docker-compose.j2 olarak düzenledim
    -  sistemin ayağa kaldırılma işlemlerini roles/wordpress-docker/tasks/main.yml dosyası altındaki adımlardan bakabilirsiniz
  # 2.2 İlk adımda dockerize edilen uygulamanın Nginx üzerinden serve edilmesi ve gelen request içerisinde bootcamp=devops header'ı varsa "Hoşgeldin Devops" statik sayfasına yönlenebilmesi
    - bu adımda nginx ayarlarının ve static sayfanın olacağı dosyaları nginx.j2 ve static.j2 olaram template klasörümde ayarladım
    - gelen request içerisindeki header check edip static sayfayı gösterecek nginx konfigurasyonunu ayarladım
```
if ($http_bootcamp = 'devops') {
    rewrite ^ /static.html break;
}
```

## Custom header check curl commands and results
```
curl --location --request GET 'http://138.68.84.22'

isteği yapıldığında normal wordpress html content return ediliyor
```

```
curl --location --request GET 'http://138.68.84.22' \
--header 'bootcamp: devops'

isteği yapıldığında 'Hosgeldin DevOps' dönecektir
```