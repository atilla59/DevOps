# Case1

## 1.1 Sanal makinada bir Centos kurup, güncellemelerinin yapılması
    - DigitalOcean servis sağlayıcı üstünden 2gb ram 50gb disk bulunan ve centos 7 kurulu bir droplet açıldı
    
    - ssh root@myip ile sunucuya erişildi
    
    - güncellemeler için "yum update" ve "yum upgrade" komutları çalıştırıldı
    
## 1.2 Kişisel bir user yaratılması (ad.soyad şeklinde) Not: aşağıdaki işlemler bu kullanıcı ile yapılacak
    - user yaratmak için "adduser atilla.pehlivan" komutu çalıştırıldı
    
    - yaratılan kullanıcının şifresi belirlenmesi için "passwd atilla.pehlivan" komutu çalıştırılıp şifresi "bootcamp" olarak ayarlandı
    
    - daha sonra yaratılan kullanıcıya geçilmek için "su atilla.pehlivan" komutu çalıştırıldı, emin olmak için "whoami" komutu ile doğrulandı
    
## 1.3. Sunucuya 2. disk olarak 10 GB disk eklenmesi ve "bootcamp" olarak mount edilmesi
    - digital ocean üzerinden sunucuya yapılandırılmamış 10GB disk ekledim
    
    - yeni bir disk eklendiği için önce "sudo mkfs.ext4 /dev/disk/by-id/scsi-0DO_Volume_bootcamp" eklenen diski ext4 formatında formatladım
    
    - daha sonra sistemde eklenen diskin hangi klasöre mount edileceğini belirlemek için "mkdir -p /mnt/bootcamp" klasörü oluşturuldu

    - daha sonra eklenen diski oluşturulan klasöre mount edildi "mount -o /dev/disk/by-id/scsi-0DO_Volume_bootcamp /mnt/bootcamp"
## 1.4. /opt/bootcamp/ altında bootcamp.txt diye bir file yaratılıp, file'ın içerisine "merhaba trendyol" yazılması

    - "atilla.pehlivan" kullanıcısının /opt dizini altında yetkisi bulunmuyor bunun için "sudo" kullanabiliriz ama önce "sudoers" dosyasına "atilla.pehlivan" kullancısnı eklememiz lazım root kullanıcınsa geçip "echo 'atilla.pehlivan ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" komutunu çalıştırıp tekrar "atilla.pehlivan" kullanıcısna geçip "sudo chmod 777 /opt" komutunu çalıştırıyoruz ardından
    
    - "mkdir -p /opt/bootcamp && touch /opt/bootcamp/bootcamp.txt && echo 'merhaba trendyol' > /opt/bootcamp/bootcamp.txt" komutuyla isterleri gerçekleştiriyoruz.
## 1.5. Kişisel kullanıcının home dizininde tek bir komut kullanarak bootcamp.txt file'ını bulup, bootcamp diskine taşınması

    - find işleminden önce mount ettiğimiz diske kullanıcı izni vermemiz gerekiyor "sudo chmod 777 /mnt/bootcamp/"

    - "find / -iname 'bootcamp.txt' -exec mv '{}' /mnt/bootcamp/ \;" daha sonra bu komutla dosyayı bulup mount edilen diske taşıyoruz

# işlemlerin yapıldığı sunucuya erişmek için web.atillapehlivan@gmail.com adresine mail atabilirsiniz.